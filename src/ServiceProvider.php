<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Tanur;

use Pusaka\Tanur\Support\TanurServiceProvider;

/**
 * ServiceProvider
 */
class ServiceProvider extends TanurServiceProvider
{

    protected $commands = [
        // Commands\NewModule::class,
    ];

    /**
     * Get provided services for deferred service provider
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
