<?php /*** Bismillahirrahmanirrahim ***/
namespace Pusaka\Tanur\Eloquent;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Traits\Macroable;

/**
 * Model
 */
class Model extends EloquentModel
{
    use Macroable {
        __call as macroCall;
    }

    /**
     * Handle dynamic method calls into the model with macro
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (static::hasMacro($method)) {
            return $this->macroCall($method, $parameters);
        }

        return parent::__call($method, $parameters);
    }

    public static function __callStatic($method, $args)
    {
        return parent::__callStatic($method, $args);
    }

    /**
     * @inheritdoc
     */
    public function getRelationValue($key)
    {
        if ($this->relationLoaded($key)) {
            return $this->relations[$key];
        }

        if (method_exists($this, $key) || static::hasMacro($key)) {
            return $this->getRelationshipFromMethod($key);
        }
    }
}
