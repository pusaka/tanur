<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Tanur\Support;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;

/**
 * TanurServiceProvider
 */
class TanurServiceProvider extends BaseServiceProvider
{
    protected $routePrefix;

    protected $commands = [];

    protected $listen = [];

    protected $subscribe = [];

    protected $autoloadResources = true;

    protected $moduleDir;

    protected $moduleId;

    private $reflector;

    /**
     * Create a new service provider instance.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->reflector = new ReflectionClass(get_class($this));

        $this->moduleNamespace = $this->getModuleNamespace();
        $this->moduleId = $this->getModuleId();
        $this->moduleDir = $this->getModuleDir();
    }

    public function getModuleNamespace()
    {
        return $this->reflector->getNamespaceName();
    }

    public function getModuleId()
    {
        $nameParts = array_slice(
            explode('\\', $this->moduleNamespace), -1
        );
        $lowerNameParts = array_map('snake_case', $nameParts);
        $moduleId = implode(':', $lowerNameParts);

        return $moduleId;
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->autoloadResources) {
            $this->loadRoutes();
            $this->loadMigrations();
            $this->loadTranslations();
            $this->loadViews();

            $this->publishConfigs();
        }

        $this->registerCommands();
        $this->bootEvents();
    }

    protected function bootEvents()
    {
        foreach ($this->listens() as $event => $listeners) {
            foreach ($listeners as $listener) {
                Event::listen($event, $listener);
            }
        }

        foreach ($this->subscribe as $subscriber) {
            Event::subscribe($subscriber);
        }
    }

    protected function listens()
    {
        return $this->listen ?? [];
    }

    public function register()
    {
        $this->mergeDefaultConfig();
    }

    /**
     * Register configuration path
     */
    protected function publishConfigs()
    { 
        $configFiles = $this->findModuleConfigFiles();

        if (empty($configFiles)) return;

        foreach ($configFiles as $configFile) {
            $basename = $configFile->getBasename();
            $destinationName = $basename == 'config.php'
                ? $this->moduleId.'.php' : $this->moduleId.'/'.$basename;

            $configs[$configFile->getRealPath()] = config_path($destinationName);
        }

        $this->publishes($configs, 'module-config');
    }

    protected function mergeDefaultConfig()
    {
        $configFiles = $this->findModuleConfigFiles();

        foreach ($configFiles as $configFile) {
            $key = $this->moduleId;
            $basename = $configFile->getBasename('.php');
            if ($basename != 'config') {
                $key .= '.'.$basename;
            }

            $this->mergeConfigFrom(
                $configFile->getRealPath(), $key
            );
        }
    }

    protected function findModuleConfigFiles()
    {
        $configPath = $this->moduleDir.'/../config';
        $isDir = File::isDirectory($configPath);

        return $isDir ? File::files($configPath) : [];
    }

    protected function loadRoutes()
    {
        /*
         * Add routes, if available
         */
        $routesPath = $this->moduleDir.'/../routes';

        if (File::isDirectory($routesPath)) {
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($routesPath)
            );
            // $iterator->setMaxDepth(1); //set max depth
            $iterator->rewind();

            while ($iterator->valid()) {
                $fileName = $iterator->getBasename();
                $realPath = $iterator->getRealPath();
                if (preg_match('~^api((_[\w]+)?_v(\d+))?\.php$~', $fileName, $m)) {
                    $version = isset($m[3]) ? $m[3] : null;
                    $this->mapApiRoutes($realPath, $version);
                }
                elseif (preg_match('~^web(_[\w]+)?\.php$~', $fileName, $m)) {
                    $this->mapWebRoutes($realPath);
                }
                elseif (preg_match('~^console|channels(_[\w]+)?\.php$~', $fileName, $m)) {
                    require_once $realPath;
                }

                $iterator->next();
            }
        }
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes($realPath, $version = null)
    {
        $namespace = $this->moduleNamespace.'\\Http\\ApiControllers';
        $prefix = '/api';

        Route::middleware('api')
             ->namespace($namespace)
             ->prefix($prefix)
             ->group($realPath);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes($realPath)
    {
        $namespace = $this->moduleNamespace.'\\Http\\Controllers';
        $prefix = $this->getRoutePrefix();

        Route::middleware('web')
             ->namespace($namespace)
             ->prefix($prefix)
             ->group($realPath);
    }

    /**
     * Returns the route prefix of a module
     *
     * @return string
     */
    public function getRoutePrefix()
    {
        $prefix = $this->routePrefix ?:
            '/'.str_replace('.', '/', $this->moduleId);

        return $prefix;
    }

    protected function loadMigrations()
    {
        $this->loadMigrationsFrom($this->moduleDir.'/../migrations');
    }

    protected function loadTranslations()
    {
        $translationsPath = $this->moduleDir.'/../resources/lang';
        $publishedTranslationsPath = resource_path('lang/vendor/'.$this->moduleId);

        $this->loadTranslationsFrom($translationsPath, $this->moduleId);
        $this->publishes([
            $translationsPath => $publishedTranslationsPath,
        ], 'module-translation');
    }

    protected function loadViews()
    {
        $viewsPath = $this->moduleDir.'/../resources/views';
        $publishedViewsPath = resource_path('views/vendor/'.$this->moduleId);

        $this->loadViewsFrom($viewsPath, $this->moduleId);
        $this->publishes([
            $viewsPath => $publishedViewsPath,
        ], 'module-view');
    }

    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }

    public function getModuleDir()
    {
        return dirname($this->reflector->getFileName());
    }
}
